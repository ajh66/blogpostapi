package com.ajh.springdemo;

public class ErrorDetails {
	private String errorMessage;
	private String devErrorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public String getDevErrorMessage() {
		return devErrorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public void setDevErrorMessage(String devErrorMessage) {
		this.devErrorMessage = devErrorMessage;
	}

}
