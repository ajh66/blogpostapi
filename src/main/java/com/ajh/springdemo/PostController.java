package com.ajh.springdemo;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/posts")
public class PostController {
	private static final Logger logger = LoggerFactory.getLogger(PostController.class);

	@Autowired
	private PostRepository postRepo;

	@Autowired
	private CommentRepository commentRepo;

	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<?> handleResourceNotFoundException(ResourceNotFoundException e)
	{
		ErrorDetails ed = new ErrorDetails();

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);

		ed.setErrorMessage(e.getMessage());
		ed.setDevErrorMessage(sw.toString());

		return new ResponseEntity<>(ed, HttpStatus.NOT_FOUND);
	}

	@GetMapping
	public List<Post> listPosts() {
		logger.info("Calling HTTP GET listPosts()");
		return postRepo.findAll();
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Post createPost(@RequestBody Post post) {
		logger.info("Calling HTTP POST createPost()");
		return postRepo.save(post);
	}

	@GetMapping(value="/{id}")
	public Post getPost(@PathVariable("id") Integer id) {
		logger.info("Calling HTTP GET getPost()");
		return postRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("No post found with id="+id));
	}

	@PutMapping(value="/{id}")
	public Post updatePost(@PathVariable("id") Integer id, @RequestBody Post post) {
		logger.info("Calling HTTP PUT updatePost()");
		postRepo.findById(id)
			.orElseThrow(() -> new ResourceNotFoundException("No post found with id="+id));
		return postRepo.save(post);
	}

	@DeleteMapping(value="/{id}")
	public void deletePost(@PathVariable("id") Integer id) {
		logger.info("Calling HTTP DELETE deletePost()");
		postRepo.findById(id)
			.orElseThrow(() -> new ResourceNotFoundException("No post found with id="+id));
		postRepo.deleteById(id);
	}

	@PostMapping(value="/{id}/comments")
	public void createPostComment(@PathVariable Integer id, @RequestBody Comment comment) {
		logger.info("Calling HTTP POST createPostComment()");
		Post post = postRepo.findById(id)
			.orElseThrow(() -> new ResourceNotFoundException("No post found with id="+id));
		post.getComments().add(comment);
	}
	
	@DeleteMapping(value="/{postId}/comments/{commentId}")
	public void deletePostComment(@PathVariable Integer postId, @PathVariable Integer commentId) {
		logger.info("Calling HTTP DELETE deletePostComment()");
		commentRepo.deleteById(commentId);
	}
}
