package com.ajh.springdemo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.DEFINED_PORT)
public class BlogPostApiApplicationTests {
	private static final Logger logger = LoggerFactory.getLogger(BlogPostApiApplicationTests.class);

	private static final String ROOT_URL = "http://localhost:8080";
	RestTemplate restTemplate = new RestTemplate();

	@Test
	public void contextLoads() {
	}

	@Test
	public void testGetAllPosts() {
		logger.info("TC - testGetAllPosts");
		ResponseEntity<Post[]> responseEntity =
				restTemplate.getForEntity(ROOT_URL+"/posts", Post[].class);

		List<Post> posts = Arrays.asList(responseEntity.getBody());

		assertNotNull(posts);
		assertEquals(posts.size(), 3);
	}

	@Test
	public void testGetPostById() {
		logger.info("TC - testGetPostById");

		Post post = restTemplate.getForObject(ROOT_URL+"/posts/1", Post.class);
		assertNotNull(post);
		assertEquals(post.getTitle(), "Introducing SpringBoot");
	}

	@Test
	public void testCreatePost() {
		logger.info("TC - testCreatePost");

		Post post = new Post();
		post.setTitle("Exploring SpringBoot REST");
		post.setContent("SpringBoot is awesome!!");
//		post.setCreatedOn(new Date());

		ResponseEntity<Post> postResponse =
				restTemplate.postForEntity(ROOT_URL+"/posts", post, Post.class);

		assertNotNull(postResponse);
		assertNotNull(postResponse.getBody());
		assertEquals(postResponse.getStatusCode(), HttpStatus.CREATED);

		logger.info(postResponse.getBody().toString());
	}

	@Test
	public void testUpdatePost() {
		logger.info("TC - testUpdatePost");

		int id = 1;
		Date date = new Date();

		Post post = restTemplate.getForObject(ROOT_URL+"/posts/"+id, Post.class);
		post.setContent("This my updated post1 content");
		post.setUpdatedOn(date);

		restTemplate.put(ROOT_URL+"/posts/"+id, post); // HTTP PUT

		Post updatedPost = restTemplate.getForObject(ROOT_URL+"/posts/"+id, Post.class);

		assertNotNull(updatedPost);
		assertEquals(updatedPost.getUpdatedOn(), date);
	}

	@Test
	public void testDeletePost() {
		logger.info("TC - testDeletePost");

		int id = 2;
		Post post = restTemplate.getForObject(ROOT_URL+"/posts/"+id, Post.class);
		assertNotNull(post);

		restTemplate.delete(ROOT_URL+"/posts/"+id);

		try {
			post = restTemplate.getForObject(ROOT_URL+"/posts/"+id, Post.class);
		}
		catch (final HttpClientErrorException e) {
			assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
		}
	}
}

